+++
title = "Welkom bij Jabber"
+++

Welkom bij de Jabbergemeenschap. Op deze pagina helpen we je een server en een client te vinden om je aan te sluiten bij het Jabbernetwerk, zodat je kunt chatten met je vrienden zonder al je gegevens weg te geven. Jabber is gedecentraliseerd, net als e-mail. Dit betekent dat je een server moet vinden of maken, zodat je van daaruit kunt chatten met alle andere gebruikers.

Dit kan in het begin verwarrend zijn, maar Jabber adressen (JIDs) zien er net zo uit als email adressen. Dus als je bijvoorbeeld de account `emma.goldman` hebt op server jabber.fr, zal je adres `emma.goldman@jabber.fr` zijn. Chatrooms (ook wel MUC's genoemd) hebben ook soortgelijke adressen, zoals `chat@joinjabber.org`.

Laten we beginnen!

# Stap 1: Registreer een account op een server

Afhankelijk van wat je wilt, kunnen wij verschillende servers aanbevelen. Klik op je gebruikssituatie om meer te weten te komen.

<div class="usecases" style="text-align: center">
<details>
<summary>
{{ usecase(usecase="personal") }}
</summary>
<p>Voor persoonlijk gebruik ben je misschien op zoek naar een non-profit aanbieder met een duurzaam verdienmodel, die meer kans heeft om over 20 jaar nog te bestaan. Wij raden je aan:</p>
{{ server(usecase="personal") }}
</details>
<details>
<summary>
{{ usecase(usecase="collective") }}
</summary>
<p>Voor je collectief/organisatie kun je <a href="https://homebrewserver.club/category/instant-messaging.html">een eigen server opzetten</a>, bijvoorbeeld met <a href="https://yunohost.org">Yunohost</a>. Als dat buiten je bereik ligt, raden we de volgende professionele hostingdiensten aan:</p>
{{ server(usecase="collective") }}
</details>
<details>
<summary>
{{ usecase(usecase="pseudo") }}
</summary>
<p>Voor politieke activiteiten zijn er veel servers die pseudonieme Jabberdiensten aanbieden. Hier zijn er een paar:</p>
{{ server(usecase="pseudo") }}
</details>
</div>



# Stap 2: Kies een client

Nu je een server gevonden hebt, kun je een client downloaden en configureren. Hier zijn de clients die wij aanbevelen voor hun kwaliteit en hun toewijding aan de behoeften van de gemeenschap. Klik op je besturingssysteem om naar een speciale documentatiepagina te gaan.

<div class="platforms" style="text-align: center">

{{ platform(platform="android") }}
{{ platform(platform="ios") }}


{{ platform(platform="gnulinux") }}
{{ platform(platform="windows") }}
{{ platform(platform="macos") }}
</div>

# Stap 3: Je bent binnen!

Gefeliciteerd! Je hebt een server en een client. Je kunt nu je client openen en het je Jabber-adres geven om verbinding te maken met de server en te chatten met anderen. Voel je je wat eenzaam? Kom dan eens kijken in de chatroom van de JoinJabber-gemeenschap: [chat@joinjabber.org](xmpp:chat@joinjabber.org?join)
