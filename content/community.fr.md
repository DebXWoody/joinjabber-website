+++
title = "Communauté"
+++

Nous sommes un nouveau collective qui souhaite aider à améliorer l'expérience utilisateurice dans l'écosystème Jabber/XMPP. Tu peux découvrir [nos objectifs](@/collective/about/goals/index.fr.md), ou lire [nos derniers comptes-rendus](@collective/_index.fr.md). Nous n'avons pas d'adhésions formelles, donc si tu sens que nous avons des intérêts en commun, ta participation est bienvenue!

Tu veux rentrer en contact? Participer au projet? Nous avons différentes chatrooms (salons) que tu peux rejoindre. Pour le moment, toutes ces chatrooms sont en Anglais, mais nous accueillons les initiatives pour en créer pour d'autres langues:

- [chat@joinjabber.org](xmpp:chat@joinjabber.org?join): discussions générales sur le collectif, et comment participer
- [abuse@joinjabber.org](xmpp:abuse@joinjabber.org?join): abus et modération dans la fédération Jabber/XMPP
- [privacy@joinjabber.org](xmpp:privacy@joinjabber.org?join): discussions sur la vie privée et la sécurité, pour utilisateurices et admins de serveurs
- [translations@joinjabber.org](xmpp:translations@joinjabber.org?join): traductions de nos ressources, pour une accessibilité mondiale
- [sysadmin@joinjabber.org](xmpp:sysadmin@joinjabber.org?join): administration système et infrastructure reproductible pour notre collectif
- [website@joinjabber.org](xmpp:website@joinjabber.org?join): comment améliorer notre site web, et comment tu peux aider
