+++
title = "Über das JoinJabber Kollektiv"
+++

In diesem Bereich kannst du die [Protokolle](https://en.wikipedia.org/wiki/Minutes) unserer Treffen finden. Falls du an unseren Zielen interessiert bist, kannst du diese [hier](@/collective/about/goals/index.md) auf English finden.
