+++
title = "Android"
template = "client.html"
[extra]
platform = "android"
clients = [ "conversations" ]
+++

On Android, the recommended client is [Conversations](https://conversations.im). You can download Conversations on [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/), the free app store for Android. If you don't have F-Droid installed yet, we strongly recommend you take the time to set it up. If you want to support the development of Conversations, you can buy it on Google's Play Store, or [donate](https://conversations.im/#donate) to the project.

![Screenshot](/conversations.jpg)

